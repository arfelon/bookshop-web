FROM jetty

RUN mkdir -p /var/lib/jetty/bookshop/db

COPY db/init/books.json /var/lib/jetty/bookshop/db/init/books.json
COPY db/init/users.json /var/lib/jetty/bookshop/db/init/users.json

COPY target/bookshop.war /var/lib/jetty/webapps/bookshop.war
