bookshop-web

Use profile <init> for initialize database or <default> for usual work

Supported only PostgreSQL :)
Open application.properties to change db settings

Accounts:
login: andrey
password: 123
type: user

login: dmitry
password: 123
type: administrator

Run project from maven:
jetty:run
or with db initialization
jetty:run -Dspring.profiles.active=init

Open url in browser:
http://localhost:8080/bookshop
