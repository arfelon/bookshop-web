<%@ page contentType="text/html;charset=utf-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>

<jsp:directive.include file="head.jsp"/>

<body>
    <section class="section">
        <jsp:directive.include file="menu.jsp"/>

        <div class="container">
            <sec:authorize access="isAuthenticated()">
                <p>Hello, <sec:authorize access="hasRole('ADMIN')">administrator </sec:authorize><b><sec:authentication property="principal.username"/></b></p>
            </sec:authorize>
            <p><s:message code="home.greeting"/></p>
        </div>
    </section>
</body>

</html>
