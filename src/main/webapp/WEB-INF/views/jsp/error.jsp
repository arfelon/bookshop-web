<%@ page contentType="text/html;charset=utf-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>

<jsp:directive.include file="head.jsp"/>

<body>
    <section class="section">
        <jsp:directive.include file="menu.jsp"/>

        <div class="container">
            <p><b>Sorry, error happened</b></p>
            <p>${message}</p>
        </div>
    </section>
</body>

</html>
