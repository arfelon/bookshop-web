<%@ page contentType="text/html;charset=utf-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<jsp:directive.include file="../head.jsp"/>

<body>
    <section class="section">
        <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a class="navbar-item" href="${ctx}"/><p class="subtitle">BookShop</p></a>
            </div>
        </nav>

        <br/>

        <div class="container">
            <c:if test="${error}">
                <div class="notification is-danger">
                  <a class="delete" href="${ctx}user/login"></a>
                  Invalid credentials
                </div>
            </c:if>
            <form method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div class="field">
                    <p class="control has-icons-left">
                        <input type="text" class="input" name="username" placeholder="Login"/>
                        <span class="icon is-small is-left"><i class="fas fa-user"></i></span>
                    </p>
                </div>

                <div class="field">
                    <p class="control has-icons-left">
                        <input type="password" class="input" name="password" placeholder="Password"/>
                        <span class="icon is-small is-left"><i class="fas fa-lock"></i></span>
                    </p>
                </div>

                <div class="field">
                    <input class="button" type="submit" value="OK">
                    <a class="button" href="${ctx}">Cancel</a>
                </div>
            </form>
        </div>
    </section>
</body>

</html>
