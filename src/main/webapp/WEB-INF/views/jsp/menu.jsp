<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="${ctx}"/><p class="subtitle">BookShop</p></a>
    </div>

    <div class="navbar-menu">
        <div class="navbar-start">
            <sec:authorize access="isAuthenticated()">
                <sec:authorize access="hasRole('ADMIN')">
                    <a class="navbar-item" href="${ctx}books/add">Add Book</a>
                </sec:authorize>
                <a class="navbar-item" href="${ctx}books">Catalog</a>
            </sec:authorize>
        </div>

        <div class="navbar-end">
            <div class="navbar-item">
                <div class="buttons">
                    <sec:authorize access="not isAuthenticated()">
                        <a class="button is-light" href="${ctx}user/login">Log in</a>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <form id="logout" method="post" action="${ctx}user/logout">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <div class="button is-light" onclick="document.getElementById('logout').submit();">Log out</div>
                        </form>
                    </sec:authorize>
                </div>
            </div>
        </div>
    </div>
</nav>

<br/>
