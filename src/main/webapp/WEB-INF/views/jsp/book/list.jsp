<%@ page contentType="text/html;charset=utf-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>

<jsp:directive.include file="../head.jsp"/>

<body>
    <section class="section">
        <jsp:directive.include file="../menu.jsp"/>

        <div class="container">
            <table class="table is-bordered is-striped is-hoverable">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Author</th>
                     </tr>
                </thead>
                <tbody>
                    <c:forEach var="book" items="${books}">
                        <tr>
                            <td><a href="${ctx}books/${book.id}">${book.title}</a></td>
                            <td>${book.author}</td>
                        </tr>
                    </c:forEach>
              </tbody>
            </table>
        </div>
    </section>
</body>

</html>
