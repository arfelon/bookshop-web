<%@ page contentType="text/html;charset=utf-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>

<jsp:directive.include file="../head.jsp"/>

<body>
    <section class="section">
        <jsp:directive.include file="../menu.jsp"/>

        <div class="container">
            <f:form method="post" enctype="multipart/form-data" action="${ctx}books/add?${_csrf.parameterName}=${_csrf.token}" modelAttribute="book">
                <f:hidden path="id" value="0"/>
                <f:hidden path="picture" value=""/>

                <div class="file has-name is-right">
                    <label class="file-label">
                        <input id="file" class="file-input" type="file" name="file" accept="image/png"/>
                        <span class="file-cta">
                            <span class="file-icon"><i class="fas fa-upload"></i></span>
                            <span class="file-label">Choose a picture…</span>
                        </span>
                        <span id="filename" class="file-name">No picture</span>
                    </label>
                </div>

                <div class="field">
                    <label class="label is-small">Title:</label>
                    <p class="control has-icons-left">
                        <f:input class="input" path="title" placeholder="Title"/>
                        <span class="icon is-small is-left"><i class="fas fa-pen"></i></span>
                    </p>
                    <f:errors path="title" cssClass="label is-small error"/>
                </div>

                <div class="field">
                    <label class="label is-small">Author:</label>
                    <p class="control has-icons-left">
                        <f:input class="input" path="author" placeholder="Author"/>
                        <span class="icon is-small is-left"><i class="fas fa-pen"></i></span>
                    </p>
                    <f:errors path="author" cssClass="label is-small error"/>
                </div>

                <div class="field">
                    <label class="label is-small">Published:</label>
                    <p class="control has-icons-left">
                        <f:input class="input" path="publicationDate" placeholder="${configProperties.datePattern}"/>
                        <span class="icon is-small is-left"><i class="fas fa-pen"></i></span>
                    </p>
                    <f:errors path="publicationDate" cssClass="label is-small error"/>
                </div>

                <div class="field">
                    <input class="button" type="submit" value="OK">
                    <a class="button" href="${ctx}">Cancel</a>
                </div>
            </f:form>
        </div>
    </section>

    <script>
        var file = document.getElementById("file");
        file.onchange = function() {
            if (file.files.length > 0) {
                document.getElementById('filename').innerHTML = file.files[0].name;
            }
        };
    </script>
</body>

</html>
