<%@ page contentType="text/html;charset=utf-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>

<jsp:directive.include file="../head.jsp"/>

<body>
    <section class="section">
        <jsp:directive.include file="../menu.jsp"/>

        <div class="container">
            <div class="box">
                <article class="media">
                    <c:if test="${not empty book.picture}">
                        <div class="media-left">
                            <figure class="image is-128x128">
                                <img alt="Picture" src="data:image/png;base64, ${book.picture}"/>
                            </figure>
                        </div>
                    </c:if>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>Title:</strong> ${book.title}
                                <br/>
                                <strong>Author:</strong> ${book.author}
                                <br/>
                                <strong>Published:</strong> <s:bind path="book.publicationDate">${status.value}</s:bind>
                            </p>
                        </div>
                        <nav class="level is-mobile">
                            <div class="level-left">
                                <a class="level-item" aria-label="back" href="${ctx}books">
                                    <span class="icon is-small">
                                        <i class="fas fa-reply" aria-hidden="true"></i>
                                    </span>
                                </a>
                                <sec:authorize access="hasRole('ADMIN')">
                                    <a class="level-item" aria-label="edit" href="${ctx}books/edit/${book.id}">
                                        <span class="icon is-small">
                                            <i class="fas fa-edit" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                    <a class="level-item" aria-label="edit" href="${ctx}books/delete/${book.id}">
                                        <span class="icon is-small">
                                            <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </sec:authorize>
                            </div>
                        </nav>
                    </div>
                </article>
            </div>
        </div>
    </section>
</body>

</html>
