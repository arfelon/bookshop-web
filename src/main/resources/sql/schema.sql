drop table if exists users;

create table users (
  id bigserial primary key not null,
  login varchar(50) not null,
  password varchar(100) not null,
  admin boolean not null,
  unique(login)
);


drop table if exists books;

create table books (
  id bigserial primary key not null,
  title varchar(50) not null,
  author varchar(50) not null,
  publication_date date not null,
  picture text
);
