package by.afanasenkov.bookshop.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/user")
public class UserController {

    @GetMapping("/login")
    public ModelAndView login(@RequestParam(required = false, defaultValue = "false") boolean error) {
        return new ModelAndView("user/login", "error", error);
    }
}
