package by.afanasenkov.bookshop.common.handler;

import by.afanasenkov.bookshop.tech.exception.ExecutionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(ExecutionException.class)
    public ModelAndView handleExecutionException(ExecutionException e) {
        return new ModelAndView("error", "message", e.getMessage());
    }
}
