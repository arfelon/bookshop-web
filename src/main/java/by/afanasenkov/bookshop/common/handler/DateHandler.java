package by.afanasenkov.bookshop.common.handler;

import by.afanasenkov.bookshop.tech.formatter.DateFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

@ControllerAdvice
public class DateHandler {

    private final DateFormatter formatter;

    @Autowired
    public DateHandler(DateFormatter formatter) {
        this.formatter = formatter;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addCustomFormatter(formatter);
    }
}
