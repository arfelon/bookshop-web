package by.afanasenkov.bookshop.book.service;

import by.afanasenkov.bookshop.book.model.Book;
import by.afanasenkov.bookshop.book.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "book")
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> list() {
        return bookRepository.findAll();
    }

    @Override
    @Cacheable(key = "#id")
    public Book get(Long id) {
        return bookRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid book id"));
    }

    @Override
    @CachePut(key = "#result.id")
    public Book add(Book book) {
        return bookRepository.save(book);
    }

    @Override
    @CachePut(key = "#result.id")
    public Book edit(Book book) {
        if (!bookRepository.existsById(book.getId())) {
            throw new IllegalArgumentException("Invalid book id");
        }

        return bookRepository.save(book);
    }

    @Override
    @CacheEvict(key = "#id")
    public void delete(Long id) {
        bookRepository.deleteById(id);
    }
}
