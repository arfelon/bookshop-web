package by.afanasenkov.bookshop.book.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "books")
public class Book implements Serializable {

    private static final long serialVersionUID = -2049993413386622211L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull(message = "Id should be specified")
    private Long id;

    @Column(name = "title")
    @NotBlank(message = "Title cannot be blank")
    private String title;

    @Column(name = "author")
    @NotBlank(message = "Author cannot be blank")
    private String author;

    @Column(name = "publication_date")
    @NotNull(message = "Publication date not set")
    @Past(message = "Publication date should be in the past")
    private LocalDate publicationDate;

    @Column(name = "picture")
    private String picture;
}
