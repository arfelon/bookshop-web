package by.afanasenkov.bookshop.book.controller;

import by.afanasenkov.bookshop.book.model.Book;
import by.afanasenkov.bookshop.book.service.BookService;
import by.afanasenkov.bookshop.tech.exception.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    private String fileToPicture(MultipartFile file) {
        try {
            return Base64Utils.encodeToString(file.getBytes());
        } catch (IOException e) {
            throw new ExecutionException("Cannot encode file to base64 string", e);
        }
    }

    @GetMapping
    public ModelAndView list() {
        return new ModelAndView("book/list", "books", bookService.list());
    }

    @GetMapping("/{id}")
    public ModelAndView get(@PathVariable Long id) {
        return new ModelAndView("book/details", "book", bookService.get(id));
    }

    @GetMapping("/add")
    public ModelAndView add() {
        return new ModelAndView("book/add", "book", new Book());
    }

    @PostMapping("/add")
    public String add(@Validated @ModelAttribute Book book, Errors result, @RequestPart MultipartFile file) {
        if (result.hasErrors()) {
            return "book/add";
        }

        book.setPicture(fileToPicture(file));
        bookService.add(book);

        return "redirect:/books";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable Long id) {
        return new ModelAndView("book/edit", "book", bookService.get(id));
    }

    @PostMapping("/edit")
    public String edit(@Validated @ModelAttribute Book book, Errors result, @RequestPart MultipartFile file, Model model) {
        if (result.hasErrors()) {
            return "book/edit";
        }

        book.setPicture(fileToPicture(file));
        bookService.edit(book);

        model.addAttribute("id", book.getId());
        return "redirect:/books/{id}";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        bookService.delete(id);
        return "redirect:/books";
    }
}
