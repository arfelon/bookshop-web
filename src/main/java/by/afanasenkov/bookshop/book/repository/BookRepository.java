package by.afanasenkov.bookshop.book.repository;

import by.afanasenkov.bookshop.book.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
