package by.afanasenkov.bookshop.tech.transaction;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Transaction {

    private boolean started;

    public void start() {
        started = true;
    }

    public void finish() {
        started = false;
    }

    public boolean isStarted() {
        return started;
    }
}
