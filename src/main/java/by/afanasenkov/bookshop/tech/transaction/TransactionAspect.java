package by.afanasenkov.bookshop.tech.transaction;

import by.afanasenkov.bookshop.config.ConfigProperties;
import by.afanasenkov.bookshop.tech.log.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.inject.Provider;

@Aspect
@Component
public class TransactionAspect {

    private final Provider<Transaction> transaction;

    private final Logger logger;

    private final ConfigProperties configProperties;

    @Autowired
    public TransactionAspect(Provider<Transaction> transaction, Logger logger, ConfigProperties configProperties) {
        this.transaction = transaction;
        this.logger = logger;
        this.configProperties = configProperties;
    }

    @Around("by.afanasenkov.bookshop.tech.pointcuts.Pointcuts.repositoryMethods()")
    public Object transactionSupport(ProceedingJoinPoint joinPoint) throws Throwable {
        Transaction t = transaction.get();

        t.start();
        if (configProperties.isDebug()) {
            logger.log("Transaction started: ", t);
        }

        try {
            return joinPoint.proceed();
        } finally {
            t.finish();
            if (configProperties.isDebug()) {
                logger.log("Transaction finished: ", t);
            }

        }
    }
}
