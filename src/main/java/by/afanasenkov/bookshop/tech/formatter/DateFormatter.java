package by.afanasenkov.bookshop.tech.formatter;

import by.afanasenkov.bookshop.config.ConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Component
public class DateFormatter implements Formatter<LocalDate> {

    private final ConfigProperties configProperties;

    @Autowired
    public DateFormatter(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    @Override
    public LocalDate parse(String text, Locale locale) {
        return LocalDate.parse(text, DateTimeFormatter.ofPattern(configProperties.getDatePattern()));
    }

    @Override
    public String print(LocalDate object, Locale locale) {
        return DateTimeFormatter.ofPattern(configProperties.getDatePattern()).format(object);
    }
}