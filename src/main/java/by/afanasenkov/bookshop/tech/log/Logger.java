package by.afanasenkov.bookshop.tech.log;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class Logger {

    public void log(String msg, Object... args) {
        StringBuilder sb = new StringBuilder();
        sb.append(msg);

        if (ArrayUtils.isNotEmpty(args)) {
            sb.append(Stream.of(args)
                    .map(String::valueOf)
                    .collect(Collectors.joining(", ")));

        }

        System.out.println(sb.toString());
    }
}
