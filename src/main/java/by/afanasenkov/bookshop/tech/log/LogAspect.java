package by.afanasenkov.bookshop.tech.log;

import by.afanasenkov.bookshop.config.ConfigProperties;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(LogAspect.LOG_PRECEDENCE)
public class LogAspect {

    public static final int LOG_PRECEDENCE = Ordered.HIGHEST_PRECEDENCE + 1;

    private static final String VOID = "void";

    private final Logger logger;

    private final ConfigProperties configProperties;

    @Autowired
    public LogAspect(Logger logger, ConfigProperties configProperties) {
        this.logger = logger;
        this.configProperties = configProperties;
    }

    private boolean isVoidRequest(JoinPoint joinPoint) {
        return ArrayUtils.isEmpty(joinPoint.getArgs());
    }

    private boolean isVoidResponse(JoinPoint joinPoint) {
        return StringUtils.equals(((MethodSignature) joinPoint.getSignature()).getReturnType().getName(), VOID);
    }

    @Before(value = "by.afanasenkov.bookshop.tech.pointcuts.Pointcuts.controllerMethods()")
    public void beforeControllerMethods(JoinPoint joinPoint) {
        if (configProperties.isDebug()) {
            if (isVoidRequest(joinPoint)) {
                logger.log("Method started " + joinPoint.getSignature());
            } else {
                logger.log("Method started " + joinPoint.getSignature() + ": ", joinPoint.getArgs());
            }
        }
    }

    @AfterReturning(pointcut = "by.afanasenkov.bookshop.tech.pointcuts.Pointcuts.controllerMethods()", returning = "response")
    public void afterControllerMethods(JoinPoint joinPoint, Object response) {
        if (configProperties.isDebug()) {
            if (isVoidResponse(joinPoint)) {
                logger.log("Method finished " + joinPoint.getSignature());
            } else {
                logger.log("Method finished " + joinPoint.getSignature() + ": ", response);
            }
        }
    }
}
