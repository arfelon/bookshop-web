package by.afanasenkov.bookshop.tech.pointcuts;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public final class Pointcuts {

    private Pointcuts() {
    }

    @Pointcut("execution(* by.afanasenkov.bookshop..controller.*.*(..))")
    public void controllerMethods() {
    }

    @Pointcut("execution(* by.afanasenkov.bookshop..repository.impl.*.*(..))")
    public void repositoryMethods() {
    }
}

